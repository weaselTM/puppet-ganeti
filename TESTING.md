# Testing

* Start the virtual machine: `vagrant up`
* Connect via ssh: `vagrant ssh`
* Now, tests can be run. For instance to add an instance:
```
gnt-instance add -t plain -o debootstrap+default --disk 0:size=5G \
  --disk 1:size=1G -B memory=512M,vcpus=1 \
  --net 0:ip=pool,network=vagrant --no-name-check --no-ip-check \
  test.local
```
  * Note: installation in nested virtualization is *quite* slow
