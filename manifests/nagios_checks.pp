# @summary Deploy optional nagios check scripts for ganeti
#
# If you include this class, make sure to order it after install of
# nagios-plugins-basic. Otherwise, the plugin directory will not exist.
#
class ganeti::nagios_checks {

  file { '/usr/local/lib/nagios/plugins/check_cluster':
    source => 'puppet:///modules/ganeti/nagios/check_cluster',
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }
  file { '/usr/local/lib/nagios/plugins/check_cluster_disks':
    source => 'puppet:///modules/ganeti/nagios/check_cluster_disks',
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }
  file { '/usr/local/lib/nagios/plugins/check_cluster_instances':
    source => 'puppet:///modules/ganeti/nagios/check_cluster_instances',
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }

}
